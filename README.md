# Restaurant API Project

Welcome to the Restaurant API project! This project aims to provide a set of APIs for managing various aspects of a small restaurant, including food items, orders, and delivery time calculations.

## Features

- **Food API**: Manage the menu by adding, updating, and deleting food items.

- **Order API**: Place and track customer orders, view order details, and update order status.

- **Delivery Time Calculation API**: Calculate estimated delivery times based on order details.

## Technologies Used

- **Django**: The web framework for building the APIs.

- **Django REST Framework (DRF)**: A powerful and flexible toolkit for building Web APIs in Django.

- **Postgres**: The default database used for simplicity.

- **Python**: The primary programming language used for backend development.

## Setup Instructions

1. Clone the repository:

   ```bash
    https://gitlab.com/aziz4ik/uzinfocom_test_task
   
- **pip install -r requirements.txt***
- ***python manage.py migrate***
- ***python manage.py runserver***

