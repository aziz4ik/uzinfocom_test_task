CREATED = 'created'
ACCEPTED = 'accepted'
SENT = 'sent'

ORDER_STATUS_CHOICES = (
    (CREATED, 'Created'),
    (ACCEPTED, 'Accepted'),
    (SENT, 'Sent')
)
