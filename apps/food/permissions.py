from rest_framework import permissions


class IsCustomer(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.user_type == 'customer'


class IsWaiter(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.user_type == 'waiter'


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.user_type == 'admin'
