from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from apps.food.models import Food
from apps.food.serializers.food import FoodModelSerializer
from apps.food.permissions import IsCustomer, IsWaiter, IsAdmin


class FoodViewSet(ModelViewSet):
    """
    API endpoint that allows
    foods to be viewed or edited
    *GET* - List all foods
    *POST* - Create a new food
    *PUT* - Update an existing food
    *DELETE* - Delete an existing food
    according to user type and permissions
    """
    model = Food
    serializer_class = FoodModelSerializer
    queryset = Food.objects.all()

    def get_permissions(self):
        if self.request.user.user_type == 'admin':
            permission_classes = [IsAdmin]
        elif self.request.method == 'GET':
            permission_classes = [IsCustomer]
        elif self.request.method in ['POST', 'PUT', 'PATCH', 'DELETE']:
            permission_classes = [IsWaiter]
        else:
            permission_classes = [permissions.AllowAny]

        return [permission() for permission in permission_classes]
