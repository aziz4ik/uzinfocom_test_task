from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.food.views import FoodViewSet

router = DefaultRouter()
router.register('foods', FoodViewSet, 'food')


urlpatterns = [
    path('', include(router.urls)),
]