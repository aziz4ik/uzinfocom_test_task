from rest_framework import serializers

from apps.food.models import Food


class FoodModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ('id', 'title', 'price', 'description', 'cooking_time')
