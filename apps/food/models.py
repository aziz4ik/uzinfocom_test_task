from django.db import models
from apps.core.models.base_model import BaseModel


class Food(BaseModel):
    title = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(max_digits=20, decimal_places=2)
    cooking_time = models.DurationField()  # in minutes

    def __str__(self):
        return self.title
