from django.contrib import admin
from apps.food.models import Food


@admin.register(Food)
class FoodAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'cooking_time')
