from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from apps.food.permissions import IsWaiter, IsCustomer, IsAdmin
from apps.order.models import Order, OrderItem
from apps.order.serializers.order import OrderModelSerializer, OrderItemModelSerializer


class OrderViewSet(ModelViewSet):
    """
    *GET* - will return all orders list according to user type
    *POST* - will create a new order
    *PUT* - will update an existing order
    *DELETE* - will delete an existing order
    """
    model = Order
    queryset = Order.objects.all()
    serializer_class = OrderModelSerializer

    def get_permissions(self):
        if self.request.user.user_type == 'admin':
            permission_classes = [permissions.AllowAny]
        elif self.request.method == 'POST':
            permission_classes = [IsCustomer]
        elif self.request.method in ['GET', 'PUT', 'PATCH']:
            permission_classes = [IsWaiter]
        else:
            permission_classes = [IsAdmin]

        return [permission() for permission in permission_classes]

    def perform_create(self, serializer):
        # Set the order user to the current user (customer)
        serializer.save(user=self.request.user)

    def create(self, request, *args, **kwargs):
        # Override the create method to handle order creation with order items
        order_data = request.data.get('order', {})
        order_items_data = request.data.get('order_items', [])

        order_serializer = self.get_serializer(data=order_data)
        order_serializer.is_valid(raise_exception=True)

        order = order_serializer.save(user=self.request.user)

        # Create order items and associate them with the order
        for order_item_data in order_items_data:
            order_item_data['order'] = order.id  # Ensure that 'order' is set to the corresponding order ID
            order_item_serializer = OrderItemModelSerializer(data=order_item_data)
            order_item_serializer.is_valid(raise_exception=True)
            order_item_serializer.save(order=order)  # Save the order item with the associated order

        order.calculate_delivery_time()
        headers = self.get_success_headers(order_serializer.data)
        return Response(order_serializer.data, status=status.HTTP_201_CREATED, headers=headers)
