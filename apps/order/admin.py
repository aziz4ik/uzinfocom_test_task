from django.contrib import admin
from .models import Order, OrderItem


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 1


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemInline]
    list_display = ('id', 'user', 'created_at', 'delivery_time', 'status')
    list_filter = ['status']
    search_fields = ('id', 'user__username')
    date_hierarchy = 'created_at'
    readonly_fields = ('id', 'created_at')
    fieldsets = (
        (None, {
            'fields': ('id', 'user', 'order_distance', 'created_at')
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        # Always make the price field read-only
        return self.readonly_fields


admin.site.register(Order, OrderAdmin)
