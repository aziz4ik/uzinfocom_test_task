from rest_framework import serializers
from apps.order.models import Order, OrderItem


class OrderItemModelSerializer(serializers.ModelSerializer):
    food_name = serializers.CharField(source='food.title', read_only=True)

    class Meta:
        model = OrderItem
        fields = ('food', 'quantity', 'food_name')


class OrderModelSerializer(serializers.ModelSerializer):
    order_items = OrderItemModelSerializer(many=True, read_only=True)
    order_distance = serializers.IntegerField(write_only=True)  # assuming order_distance is an IntegerField

    class Meta:
        model = Order
        fields = ('id', 'status', 'created_at', 'order_distance', 'order_items', 'delivery_time')

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        # Explicitly include order items data in the representation
        representation['order_items'] = OrderItemModelSerializer(instance.orderitem_set.all(), many=True).data
        return representation
