from math import ceil
from django.db import models
from django.utils import timezone

from apps.core.constants.order_statuses import ORDER_STATUS_CHOICES
from apps.core.models.base_model import BaseModel
from apps.food.models import Food
from apps.user.models import User


class Order(BaseModel):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    status = models.CharField(max_length=55, choices=ORDER_STATUS_CHOICES, default='created')
    delivery_time = models.DurationField(null=True, blank=True)
    order_distance = models.PositiveIntegerField(null=True, blank=True)

    def calculate_delivery_time(self):
        if self.order_distance is not None:
            # Count the total quantity of food items in the order
            total_quantity = sum(item.quantity for item in self.orderitem_set.all())

            # Calculate the number of 4-food batches needed to cook all the food items
            cooking_batches = ceil(total_quantity / 4)

            # Calculate the cooking duration in minutes
            cooking_duration = cooking_batches * 5

            # Calculate delivery time based on distance
            delivery_duration = self.order_distance * 3  # Assuming 3 minutes per km

            # Update the delivery_time field
            self.delivery_time = timezone.timedelta(minutes=cooking_duration + delivery_duration)
            self.save()
        else:
            # If order_distance is None, set delivery_time to None
            self.delivery_time = None
            self.save()

    def __str__(self):
        return f'{self.id}'


class OrderItem(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    food = models.ForeignKey(Food, on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.order.id}'
