from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/user/', include('apps.user.urls')),
    path('api/food/', include('apps.food.urls')),
    path('api/order/', include('apps.order.urls')),
]
